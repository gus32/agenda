package business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import com.example.demo.dao.Connection;

import entity.Paciente;

@Controller
@Configuration
public class Bd {
	
	
	
	@Autowired
	Connection conecction;
	
	public String insertarPaciente(Paciente paciente) {
		return conecction.insertar(paciente);
	}
	public String consultarPaciente(String nombre) {
		return conecction.consultar(nombre);
	}
	public String actualizarPaciente(String nombre) {
		return conecction.actualizar(nombre);
	}
	public String eliminarPaciente(String nombre) {
		return conecction.eliminar(nombre);
	}
	
}
