package com.example.demo.dao;

import org.springframework.stereotype.Service;

import entity.Paciente;

@Service
public class Connection {
	
	public String insertar (Paciente paciente) {
		return "El paciente "+paciente.getNombre()+" "+paciente.getApellido()+" fue registrado con el id: "+paciente.getId();
	}
	public String consultar (String nombre) {
		return "El paciente "+nombre+" fue consultado";
	}
	public String actualizar(String nombre) {
		return "El paciente "+nombre+" fue actualizado";
	}
	public String eliminar (String nombre) {
		return "El paciente "+nombre+" fue eliminado";
	}
}
