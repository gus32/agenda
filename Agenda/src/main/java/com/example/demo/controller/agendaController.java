package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import business.Bd;
import entity.Paciente;

@RestController
public class agendaController {
	
	Bd bd = new Bd();
	@PostMapping("/paciente")
	public String postPaciente (@RequestBody Paciente paciente) {
		return bd.insertarPaciente(paciente) ;
	}
//	@RequestMapping("/paciente2/${nombre}")
//	public void getPaciente(@PathVariable String nombre) {
//		System.out.println(bd.consultarPaciente(nombre));
//	}
//	
//	@PutMapping("/paciente3/${nombre}")
//	public void putPaciente (@PathVariable String nombre) {
//		System.out.println(bd.actualizarPaciente(nombre));
//	}
//	@DeleteMapping("/paciente4/${nombre}")
//	public void deletePaciente (@PathVariable String nombre) {
//		System.out.println(bd.eliminarPaciente(nombre));
//	}
}
